import time
import concurrent.futures
import os
from urllib.parse import urlparse
import io
import ijson
import pandas as pd
import subprocess
import sys
import logging
import requests
import csv

# Receives a shell command as arg and then execute it using the subprocess lib
def exec_shell(command_arr):
    process = subprocess.Popen(command_arr,
                     stdout=subprocess.PIPE, 
                     stderr=subprocess.PIPE)
    #it returns a tuple, thats why whe are using like this...
    stdout, stderr = process.communicate()
    return stdout,stderr    

def get_arr_download(file_name,path):    
    return ['curl',file_name ,'--output',path]
 
def get_arr_extract_file(file_name):
    return ['gunzip',file_name]

def get_arr_delete_file(file_name):
    return ['rm','-rf',file_name]

def generate_minimal_csv(filename):        
    traces =[]
    file_name = filename.replace('.json','.csv')

    f = open(file_name, 'w')

    # using csv.writer method from CSV package
    write = csv.writer(f)      
    write.writerow(['start_time','end_time', 'collection_id','instance_index','average_usage.cpus','average_usage.memory'])
    chunk_size = 1000
    i =0
    with open(filename, encoding="UTF-8") as json_file:
        cursor = 0
        for line_number, line in enumerate(json_file):
            row = []
            line_as_file = io.StringIO(line)
            collection_type = 0
            # Use a new parser for each line
            json_parser = ijson.parse(line_as_file)
            for prefix, type, value in json_parser:
                if(prefix == 'start_time'):
                    row.append(int(value))
                elif(prefix == 'end_time'):
                    row.append(int(value))
                elif(prefix == 'collection_id'):
                    row.append(value)
                elif(prefix == 'instance_index'):
                    row.append(value)
                elif(prefix == 'collection_type'):
                    collection_type = int(value)
                elif(prefix == 'average_usage.cpus'):
                    row.append(float(value))
                elif(prefix == 'average_usage.memory'):
                    row.append(float(value))
            cursor += len(line)
            #only consider data from jobs
            if(collection_type == 0):
                traces.append(row)
            if(i > chunk_size):            
                write.writerows(traces)                               
                traces =[]
                i = 0
            i = i+1
    if(len(traces)>0):
        write.writerows(traces)                               
    del traces
    f.close()
    logging.info(f'CSV file {file_name} created')

def get_files(cell, table,index):    
    url = 'https://www.googleapis.com/storage/v1/b/clusterdata_2019_'+cell+'/o?'
    response = requests.get(url)
    json_var = response.json()
    files = []
    for item in json_var['items']:
        if table in item['name']:
            files.append('https://storage.googleapis.com/clusterdata_2019_'+cell+'/' +item['name'] )            
    size = int (len(files) / 8 ) 
    i = index * size
    j = (index+1) * size
    if(index == 7):
        j = len(files)            
    logging.info(f"Those indexes will be processed: {i}, {j}")
    return files[i:j]
    
def download_file(file_name,path):
    logging.info(f'Downloading file {file_name} ...')    
    return exec_shell(get_arr_download(file_name,path))

def extrac_gz_file(file_name):
    logging.info(f'Extracting file {file_name} ...')
    return exec_shell(get_arr_extract_file(file_name))




def delete_json_file(file_name):
    logging.info(f'Deleting file {file_name} ...')
    return exec_shell(get_arr_delete_file(file_name))

def processFile(file_name):      
    logging.info(f'Processing file {file_name}...') 
    url_ = urlparse(file_name)
    path = os.path.basename(url_.path)    
    download_file(file_name,path)       
    out_,err_ = extrac_gz_file(path)    
    newFileName = path.replace('.gz','')
    generate_minimal_csv(newFileName)
    delete_json_file(newFileName)    
    logging.info(f'File {newFileName} processed!') 

def run(cell, table,index):
    files = get_files(cell, table,index)
    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ProcessPoolExecutor() as executor:
        # Start the load operations and mark each future with its URL
        future_to_url = {executor.submit(processFile, file_name): file_name for file_name in files}
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
            except Exception as exc:
                logging.info('%r generated an exception: %s' % (url, exc))        


logging.basicConfig(filename='LOGFILE.log', level=logging.DEBUG)
start = time.perf_counter()
cell = sys.argv[1]
index = int(sys.argv[2])
run(cell,'instance_usage',index)
finish = time.perf_counter()
logging.info(f'finished in {round(finish-start,2)} second(s)')
